#!/bin/bash

for SRA in $(ls SRA/*)
do

fastq-dump --gzip --split-3 $SRA &

done
