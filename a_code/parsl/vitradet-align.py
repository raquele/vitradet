import parsl
import os
from parsl.app.app import python_app, bash_app
from parsl.data_provider.files import File

# === Uncomment this to see logging info ===
import logging
logging.basicConfig(level=logging.DEBUG)

# === config execution provider ===
#from parsl.configs.local_threads import config
from sdumont_mesca2 import config
parsl.load(config)

# ================================
# === Directories organization ===
# ================================
genome_dir = '/scratch/vitradet/raquel.costa2/genome/Homo_sapiens/USCS/hg19/WholeGenome'
genome_prefix = genome_dir + '/rsem_ref_hg19'

data_input = '/scratch/vitradet/raquel.costa2/data/hcc_tcga/raw_data/file_bam/'
viral_db = '/scratch/vitradet/raquel.costa2/virus_db/viralConcatened'

results_fastq = '/scratch/vitradet/raquel.costa2/results/hcc_tcga/results_fastq/'
results_fastqpaired = '/scratch/vitradet/raquel.costa2/results/hcc_tcga/results_fastq_paired/'
results_mapped = '/scratch/vitradet/raquel.costa2/results/hcc_tcga/results_rsem/'
results_virmapped = '/scratch/vitradet/raquel.costa2/results/hcc_tcga/results_bowtie2/'
#results_unmapped = '/scratch/vitradet/raquel.costa2/results/hcc_tcga/results_unmapped'
#results_mapped = '/scratch/vitradet/raquel.costa2/results/hcc_tcga/results_mapped'
#results_qc = '/scratch/vitradet/raquel.costa2/results/hcc_tcga/results_qc'
#results_metadata = '/scratch/vitradet/raquel.costa2/results/hcc_tcga/results_metadata'

# number of threads in tasks with internal parallelization
nthreads = 24

# enabled script executors
script_executors_multithread = ['sdumont_htex_mesca2_1w']
script_executors_singlethread = ['sdumont_htex_mesca2_1w']
# ======================================
# === Functions associated with APPs ===
# ======================================

# ---------------------------
# *** PART II - ALIGNMENT ***
# ---------------------------
# === Aligment using RSEM ===
@bash_app(executors=script_executors_multithread)
def alignment(nthreads, sample_name, genome_prefix, inputs=[], outputs=[], stderr=parsl.AUTO_LOGNAME, stdout=parsl.AUTO_LOGNAME):
    return "rsem-calculate-expression --paired-end --bowtie2 -p {} --output-genome-bam --keep-intermediate-files --append-names --estimate-rspd --time {} {} {} {};".format(nthreads, inputs[0], inputs[1], genome_prefix, sample_name)

# ---------------------------------
# *** PART III - UNMAPPED READS ***
# ---------------------------------
# === Unmapped reads ===
# Parameters: bowtie2 -x <bowtie_ref> --local -1 <seq_p1.fq> -2 <seq_p2.fq> --threads <num_threads> --al <out.align> --un <out.unaligned> -S <out.sam>
@bash_app(executors=script_executors_multithread)
def alignmentVirMapped(nthreads, inputs=[], outputs=[], stderr=parsl.AUTO_LOGNAME, stdout=parsl.AUTO_LOGNAME):
    return "bowtie2 -x {} --local -1 {} -2 {} --threads {} --al {} --un {} -S {} --sensitive".format(inputs[0], inputs[1], inputs[2], nthreads, outputs[0], outputs[1], outputs[2])

# =====================
# === Workflow body ===
# =====================

alignment_futures = []
for sra_file in os.scandir(data_input):
    fastq_paired_r1 = File(results_fastqpaired + sra_file.name.replace(".bam", "_r1.fastq"))
    fastq_paired_r2 = File(results_fastqpaired + sra_file.name.replace(".bam", "_r2.fastq"))
    sample_name = os.path.basename(fastq_paired_r1.filepath.split('_gdc', 1)[0])
    alignment_output_file_un1=File(os.path.join(results_mapped + sample_name + ".temp", sample_name + "_un_1.fq"))
    alignment_output_file_un2=File(os.path.join(results_mapped + sample_name + ".temp", sample_name + "_un_2.fq"))
    alignment_futures.append(alignment(nthreads, results_mapped + sample_name, genome_prefix, inputs=[fastq_paired_r1, fastq_paired_r2], outputs=[alignment_output_file_un1, alignment_output_file_un2]))

rsem_align_files = [(r.outputs[0], r.outputs[1]) for r in alignment_futures]
viralign_futures = []
for rsem_align_file_un1, rsem_align_file_un2 in rsem_align_files:
    sample_name = os.path.basename(rsem_align_file_un1.filepath.split('_un_1.fq', 1)[0])
    alignment_output_align=File(os.path.join(results_virmapped, sample_name + ".align"))
    alignment_output_unaligned=File(os.path.join(results_virmapped, sample_name + ".unaligned"))
    alignment_output_sam=File(os.path.join(results_virmapped, sample_name + ".sam"))
    viralign_futures.append(alignmentVirMapped(nthreads, inputs=[viral_db, rsem_align_file_un1, rsem_align_file_un2], outputs=[alignment_output_align, alignment_output_unaligned, alignment_output_sam]))

for r in viralign_futures:
    print(r.outputs[0].result().filepath)
