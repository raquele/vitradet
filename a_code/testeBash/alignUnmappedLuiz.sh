#!/bin/bash
#SBATCH --nodes=3
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task=24
#SBATCH -p cpu_dev
#SBATCH -J vitradet


module load samtools/1.9
module load bowtie2/2.3

NTHREADS=24
NODES=3

let srun_count=0
echo "srun_count=$srun_count"

# ===================
# === DIRECTORIES ===
# ===================
N_THREAD=24


DIR_IN=/scratch/vitradet/raquel.costa2/results/hcc_tcga/teste_batch_RSem
DIR_OUT=/scratch/vitradet/raquel.costa2/results/hcc_tcga/teste_batch_virusMapped
DIR_VIRALDB=/scratch/vitradet/raquel.costa2/virus_db/virus.bowtie2.refB

cd $DIR_IN

# =================
# === FUNCTIONS ===
# =================
# === Bowtie2 ===

for i in $(ls *r1.fastq | awk -F "r1.fastq" '{print $1}' | uniq)
do
 echo "-----------------------------------------------"
 echo $i
 date

 srun -N 1 -n 1 -c 24 bowtie2 -x $DIR_VIRALDB --local -1 $DIR_IN\.temp/"_un_1.fq" -2 $DIR_IN\.temp/"_un_1.fq" --threads $N_THREAD --all $DIR_OUT/$ID".bowtie2.align" --un $DIR_OUT/$ID".Bowtie2.unaligned" -S $DIR_OUT/$ID".bowtie2.sam" --sensitive > $DIR_OUT$ID/$ERR2 2> $DIR_OUT$ID/$ERR3 &
 termediate-files --append-names --estimate-rspd --time $i"r1.fastq" $i"r2.fastq" $DIR_GENOME/rsem_ref_hg19 $DIR_OUT/$i &
 #rsem-calculate-expression --paired-end --bowtie2 -p 24 --output-genome-bam --keep-intermediate-files --append-names --estimate-rspd --time $i\_1.fastq $i\_2.fastq /home/quelopes/Data_Buriti/Genome/Homo_sapiens/USCS/hg19/WholeGenome/rsem_ref_hg19 $DIR_BOWTIE_ALIGNED/$i

 let srun_count++
 echo "srun_count=$srun_count"
 if [ "$srun_count" -eq "$NODES" ]
   then
  wait
        let srun_count=0
 fi
 #echo "Finished alignment of $i to reference genome..."
 done
 if [ "$srun_count" -gt "0" -a "$srun_count" -lt "$NODES" ]
  then
  wait
 fi
