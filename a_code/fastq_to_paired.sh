#!/bin/bash
# extracting reads ending with '/1' or '/2'
cat $1 | grep '^@.*/1$' -A 3 --no-group-separator > $2
cat $1 | grep '^@.*/2$' -A 3 --no-group-separator > $3
exit 0
