import parsl
import os
from parsl.app.app import python_app, bash_app
from parsl.data_provider.files import File

# === Uncomment this to see logging info ===
import logging
logging.basicConfig(level=logging.DEBUG)

# === config execution provider ===
#from parsl.configs.local_threads import config
from sdumont_mesca2 import config
parsl.load(config)

# ================================
# === Directories organization ===
# ================================
genome_dir = '/scratch/vitradet/raquel.costa2/genome/Homo_sapiens/USCS/hg19/WholeGenome'
genome_prefix = genome_dir + '/rsem_ref_hg19'

data_input = '/scratch/vitradet/raquel.costa2/data/hcc_tcga/raw_data/file_bam/'
viral_db = '/scratch/vitradet/raquel.costa2/virus_db/viralConcatened'

results_fastq = '/scratch/vitradet/raquel.costa2/results/hcc_tcga/results_fastq/'
results_fastqpaired = '/scratch/vitradet/raquel.costa2/results/hcc_tcga/results_fastq_paired/'
results_mapped = '/scratch/vitradet/raquel.costa2/results/hcc_tcga/results_rsem/'
results_virmapped = '/scratch/vitradet/raquel.costa2/results/hcc_tcga/results_bowtie2/'
results_virmapped_complete = '/scratch/vitradet/raquel.costa2/results/hcc_tcga/results_bowtie2_complete/'
results_sam_to_bam = '/scratch/vitradet/raquel.costa2/results/hcc_tcga/results_sam_to_bam/'
results_sort_bam = '/scratch/vitradet/raquel.costa2/results/hcc_tcga/results_sort_bam/'
#results_unmapped = '/scratch/vitradet/raquel.costa2/results/hcc_tcga/results_unmapped'
#results_mapped = '/scratch/vitradet/raquel.costa2/results/hcc_tcga/results_mapped'
#results_qc = '/scratch/vitradet/raquel.costa2/results/hcc_tcga/results_qc'
#results_metadata = '/scratch/vitradet/raquel.costa2/results/hcc_tcga/results_metadata'

# number of threads in tasks with internal parallelization
nthreads = 24

# enabled script executors
script_executors_multithread = ['sdumont_htex_mesca2_1w']
script_executors_singlethread = ['sdumont_htex_mesca2_1w']
# ======================================
# === Functions associated with APPs ===
# ======================================


# ---------------------------------
# *** PART III - UNMAPPED READS ***
# ---------------------------------
# === Unmapped reads ===
# Parameters: bowtie2 -x <bowtie_ref> --local -1 <seq_p1.fq> -2 <seq_p2.fq> --threads <num_threads> --al <out.align> --un <out.unaligned> -S <out.sam>

@bash_app(executors=script_executors_multithread)
def convertSamtoBam(inputs=[], outputs=[], stderr=parsl.AUTO_LOGNAME, stdout=parsl.AUTO_LOGNAME):
    return "samtools view -bS {} > {}".format(inputs[0], outputs[0])

@bash_app(executors=script_executors_multithread)
def sortBam(nthreads, inputs=[], outputs=[], stderr=parsl.AUTO_LOGNAME, stdout=parsl.AUTO_LOGNAME):
    return "samtools sort {} -o {} -@ {}".format(inputs[0], outputs[0], nthreads)

@bash_app(executors=script_executors_multithread)
def indexBam(inputs=[], outputs=[], stderr=parsl.AUTO_LOGNAME, stdout=parsl.AUTO_LOGNAME):
    # the index file has extension .index
    return "samtools index {}".format(inputs[0])

@bash_app(executors=script_executors_multithread)
def idxstatsBam(inputs=[], outputs=[], stderr=parsl.AUTO_LOGNAME, stdout=parsl.AUTO_LOGNAME):
    return "samtools idxstats {} > {}".format(inputs[0], outputs[0])

# =====================
# === Workflow body ===
# =====================

convertSamtoBam_futures = []
for sra_file in os.scandir(results_virmapped_complete):
    if sra_file.is_file() and sra_file.name.endswith(".sam"):
        sample_name = sra_file.name.split('.sam', 1)[0]
        sam_file = File(os.path.join(results_virmapped_complete, sample_name + ".sam"))
        bam_file_output=File(os.path.join(results_sam_to_bam, sample_name + ".bam"))
        convertSamtoBam_futures.append(convertSamtoBam(inputs=[sam_file], outputs=[bam_file_output]))

sam_files = [r.outputs[0] for r in convertSamtoBam_futures]
sortBam_futures = []
for sam_file in sam_files:
    sorted_bam_filename = os.path.basename(sam_file.filepath.replace('.bam', '.sorted.bam'))
    sorted_bam_file = File(os.path.join(results_sort_bam, sorted_bam_filename))
    sortBam_futures.append(sortBam(nthreads, inputs=[sam_file], outputs=[sorted_bam_file]))

sorted_bam_files = [r.outputs[0] for r in sortBam_futures]
indexBam_futures = []
for sorted_bam_file in sorted_bam_files:
    indexed_bam_filename = os.path.basename(sorted_bam_file.filepath.replace('.bam', '.index'))
    indexed_bam_file = File(os.path.join(results_sort_bam, indexed_bam_filename))
    indexBam_futures.append(indexBam(inputs=[sorted_bam_file], outputs=[indexed_bam_file]))

indexed_bam_files = [r.outputs[0] for r in indexBam_futures]
idxstatsBam_futures = []
for indexed_bam_file in indexed_bam_files:
    idxstats_bam_filename = os.path.basename(indexed_bam_file.filepath.replace('.index', 'idxstats.txt'))
    idxstats_bam_file = File(os.path.join(results_sort_bam, idxstats_bam_filename))
    sorted_bam_filename = os.path.basename(indexed_bam_file.filepath.replace('.index', '.sorted.bam'))
    sorted_bam_file = File(os.path.join(results_sort_bam, sorted_bam_filename))
    idxstatsBam_futures.append(idxstatsBam(inputs=[sorted_bam_file, indexed_bam_file], outputs=[idxstats_bam_file]))

for r in idxstatsBam_futures:
    print(r.outputs[0].result().filepath)
