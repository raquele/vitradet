---
title: "ViTraDet - Viral Transcription Detection from Human RNA-seq data"
header-includes:
  - \usepackage{titling}
  - \pretitle{\begin{center}
    \includegraphics[width=2in,height=2in]{../vitradet_part_ii/figures/vitradet_logo.png}\LARGE\\}
  - \posttitle{\end{center}}
output:
  pdf_document:
    toc: yes
    toc_depth: '4'
    fig_caption: yes
  html_document:
    toc: yes
    toc_depth: 4
---

<!-- 
==================
*** BACKGROUND ***
==================
---> 
# --- BACKGROUND ---

Virus and cancer. 
The execution time of traditional pipelines does not take into account the parallelism of activities.

<!-- 
=========================
*** DATA ORGANIZATION ***
=========================
---> 
# Data organization 

``` 
/vitradet_git/
|--- README.md             
|--- a_code/               
```
a_code: codes, such as pipeline `vitradet.py` sdumont configurations `sdumont_cpu_small.py`.


```
/vitradet_data/           
data/hcc_tcga/raw_data/testeBam 
```

testeBam: todos os arquivos .bam -- 555Gb

```
/vitradet_results/
```


```
/genome/Homo_sapiens/USCS/hg19/WholeGenome/
```

```
/vitradet_other_parts/
|--- figures
|--- documents            
```



## On the Santos Dumont supercomputer

**OUTPUT**

* **Results fastq**: \ 
    `/scratch/vitradet/raquel.costa2/results/hcc_tcga/results_fastq` \
    file transformed to fastq 1.4Tb + fastq paired file 1.4Tb
* **Result QC**: \ 
    `/scratch/vitradet/raquel.costa2/results/hcc_tcga/results_qc`
* **Result mapped**: \  
    `/scratch/vitradet/raquel.costa2/results/hcc_tcga/results_mapped` \
    Volume: 2.5Tb
* **Result unmapped**: \
    `/scratch/vitradet/raquel.costa2/results/hcc_tcga/results_unmapped` \
    Volume: 1.4Tb
* **Result metadata**: \
    `/scratch/vitradet/raquel.costa2/results/hcc_tcga/results_metadata`
    
   
**TEST BATCH**

* **Results fastq**: \ 
    `/scratch/vitradet/raquel.costa2/results/hcc_tcga/teste_Bam`
* **Result QC**: \ 
    `/scratch/vitradet/raquel.costa2/results/hcc_tcga/teste_batch_FastPareid`
* **Result mapped**: \  
    `/scratch/vitradet/raquel.costa2/results/hcc_tcga/teste_RSem`

    
**TEST PARSL**

* **Results fastq**: \ 
    `/scratch/vitradet/raquel.costa2/results/hcc_tcga/teste_Bam`
* **Result QC**: \ 
    `/scratch/vitradet/raquel.costa2/results/hcc_tcga/teste_batch_FastPareid`
* **Result mapped**: \  
    `/scratch/vitradet/raquel.costa2/results/hcc_tcga/teste_RSem`


<!-- 
================
*** WORKFLOW ***
================
---> 
# --- WORKFLOW ---

Build in Parsl. Parsl is a library written in Python for executing workflows in high performance computing resources.


![ViTraDet workfow.](../vitradet_part_ii/figures/ViTraDet_workflow.png){width=70%}



<!-- 
----------------
--- Activity 1 ---
----------------
---> 
# Activity 1: Data acquisition

RNA-Seq data can be obtained from different sources. In the Activity `Data acquisition` ViTraDet was implemented with three different forms of the data acquisition which are explained below.


## A - From TCGA with restrict access

In this Activity, only projects with permission of the restrict access from the TCGA are be able to executed it. 
The download data from TCGA are in `.bam` format containing reads already aligned. In this step, the data need to return in the .fastq format to further analysis.

How download TCGA data:

- go to GDC portal and select the itens to put into the cart.
- go to cart and save as `gdc_sample_sheet_data.tsv`. This archive contain the number of the patient and the reference archive `.bam`.
- to download the tcga archive you need a token. download `gdc-client` and manifest.


### Bam to Fastq


### SRA to fastq


* SRA_to_fastq.sh
=> gera diretorio gzSRA 



**Observations**:



<!-- 
--- From SRA ---
---> 
## B - From SRA repository

**B.1** - First of all, you need install `aria2c`.

Install aria2c \
In Linux (Debian distribution): 

* `$ sudo apt-get install aria2c`

Or you can see in this [link](https://aria2.github.io/) https://aria2.github.io/


**B.2** - Second, you need create a archive contains the samples to download:

necessario um arquivo `url-Miguel-INCA.txt`

Como construir o arquivo `.txt` \ 
ir em BioSample e exportar como: `file -> bioSampleIDlist`

**B.3** - Finally, you execute the follow script:

* `$ ./get_SRA.sh url.txt`

Results: gzSRA directory.


  

<!--
--- From Sequencing ---
---> 
## C - From sequencing


<!-- 
----------------
--- Activity 2 ---
----------------
---> 
# Activity 2: Quality control -- QC (optional) 


Removing some virus contamination in the samples (see some articles about it)



<!-- 
----------------
--- Activity 3 ---
----------------
---> 
# Activity 3: Alignment

## Activity 3a: Reference genome and indexing

## Activity 3b: Alignment 

Ver: 

* firstStep_RSEM.sh 
* firstStep_STAR.sh (dois passos) \
  [link ver](https://software.broadinstitute.org/gatk/documentation/article?id=3891)
* firstStep_BWA.sh
          
Todo pipe in shell (amostras de rnaSeq cervical): \
`nohup /home/quelopes/Dev_Buriti/VirusIntegration/ViTraDet/aligUnmapped.sh samplenames.txt &`


<!-- 
----------------
--- Activity 4 ---
----------------
---> 
# Activity 4: Unmapped analysis


Curated data virus integration (NAR, 2020)


## Activity 4a: Preparing the viral reference

Download the last viral reference (two dataset *.fna* in november 2020): 

* [dataset 1:](ftp://ftp.ncbi.nih.gov/refseq/release/viral/)
wget ftp://ftp.ncbi.nlm.nih.gov/refseq/release/viral/viral.1.1.genomic.fna.gz

* [dataset 2:](ftp://ftp.ncbi.nih.gov/refseq/release/viral/)
wget ftp://ftp.ncbi.nlm.nih.gov/refseq/release/viral/viral.2.1.genomic.fna.gz

https://www.cureffi.org/2013/01/25/aligning-unmapped-reads-to-viral-genomes/

Using this following command in order to concatate the archives:

    cat viral.1.1.genomic.fna viral.2.1.genomic.fna > viralConcatened.fna
    
**Obs**: Two dataset: all virus and selected virus
Selected DB: "Human", "papilomavirus" or "hepatitis"


Para remover sequencias virais pois o arquivo concatenado as vezes fica mt grande:

`from Bio import SeqIO
for record in SeqIO.parse("viralConcatened.fna","fasta"):
   if "hepatitis" in record.description or "Hepatitis"
   print(record.format("fasta"))
   
$ python3 alignUnmappedLuiz.teste.sh
    
See more information: 

* [link to journal: https://www.nature.com/articles/nbt.4060](https://www.nature.com/articles/nbt.4060)
* [link to question: https://www.biostars.org/p/356134/](https://www.biostars.org/p/356134/)
* [link to virusDetectionSRA: https://github.com/NCBI-Hackathons/Virus_Detection_SRA](https://github.com/NCBI-Hackathons/Virus_Detection_SRA)  
* [link aligning unmapped](https://www.cureffi.org/2013/01/25/aligning-unmapped-reads-to-viral-genomes/)

### Annotation about the virus 

[Download the table from NCBI: https://www.ncbi.nlm.nih.gov/genomes/GenomesGroup.cgi?taxid=10239](https://www.ncbi.nlm.nih.gov/genomes/GenomesGroup.cgi?taxid=10239)

    taxid10239.tbl
    
Observacao: essa tabela precisa ser trabalhada antes de ser utilizada. Colocar a tabela trabalhada como anexos.
Tive que remover nomes de familias, e ajustar colunas.


é preciso ajustar o ID number do db viral. Remover tudo que tem .XX (onde XX é um numero) do accession number. 

## Indexing the viral reference file to Bowtie2

    bowtie2-build -p viralConcatenedFiltered.fna viralConcatenedFiltered.ref
    
## Activity 4b: Alignment the unmapped reads in viral reference

Function in the script `functionAligmentUnmapAndViralRef()`
         
         bowtie2 -x $VIRAL_REFERENCE --local only_unmapped.fastq --threads N_THREAD \
         --al resAligUnmapWithVir.bowtie2.align --un res.Bowtie2.unaligned \
         -S res.bowtie2.sam --sensitive

## Converting and sorting

Functions in the script `functionConvertSamtoBam()` and `functionSortBam()`
      
## Indexing

O arquivo idxstats.txt tab delimited file which each line:
a) virus sequence name
b) sequence length
c) mapped reads
d) unmapped reads

      Organiza os resultados das amostras `mergeIdx.R`
      
      functionIndexBam()
      functionIdxStatBam()


<!-- 
----------------
--- Activity 5 ---
----------------
---> 
# Activity 5: Mapped analysis 


Avaliar ainda:

* variant calling ?
* gene fusion ?



<!-- 
================
*** DATASETS ***
================
---> 
# --- Datasets ---

## RNA-Seq

### TCGA restrict access

* biological context: hepatocarcinoma associated with HCV and HBV infections
* accession number: 
* id: `Phs 000178.v10.p8`
* number of samples: 75 samples
* volume: 544 Gb
* location: `/home/quelopes/data_vitradet/hcc_tcga/raw_data/file_bam`
* location: `/scratch/vitradet/raquel.costa2/data_vitradet/hcc_tcga/raw_data/file_bam`

### SRA repository

* biological context: cervical cancer with papilomavirus
* accession number: `GSE91065`
* number of samples: 42 samples
* volume: 46 Gb
*location: `/scratch/vitradet/raquel.costa2/data_vitradet/hpv_inca/gzSRA`

## Viruses




<!--
To show the efficiency of your solution, we conducted two different experiments in cancers associated with virus infection. The first one was cervical cancers which are associated with HPV infection and the second one was hepatocarcinome which is associated with HCV and HBV infections. Below a short description of each study case.

## Hepatocarcinoma

Hepatocelular carcinoma (HCC) is the second cancer with highest motality rate in the world ~\cite{Ally2017}. 
The main etiology with HCC are related with the replication of the Hepatitis C virus and Hepatitis B virus in the hepatocitos where the chronic infection by HCV are most prevalent in the world~\cite{Arzumanyan2013, Vescovo2016}.

HCV 9.600 nt. Hepacivirus framily Flaviviridae. Single-stranded. Positive-sense RNA.
Polyprotein 3.000aa.

Alternative reading frame protein or F (Frameshft) or C (core+1).

N terminal region the polyprotein is processed by cellular proteases to yield the structural-protein core (C) and envelope proteins 1 and 2 (E1 and E2).
Polyprotein processed by celular and viral proteases into at least 10 structural and nonstructural viral proteins. Some works suggest that core+1 protein plays a role in advanced live disease and liver cancer. Vassilaki 2009: various forms of the core+1. Cicle of the inflamation, necrose, regeneracao...levando a uma instabilidade cromossomal.


Core+1 nao afeta o ciclo de replicacao viral.
HCV 7 genotypes (1,2 and 3 are worldwide) and more than 100 subtypes. 
In 1998 the first evidence of the expression of a protein from an alternative ORF overlaping the core encoding region in the +1 frame. (Valaki 2008)

Ja foram detectados anti-core+1 antibodies and TCell responses in HCV-infection.
HCV +sRNA polyprotein 3000 aa -> processed -> Warburg effect

non-structural proteins are related with replication and particle assembly


TCGA dados em formato .bam foram transformados para fastq e em seguida separado os reads.

Eventos moleculares na formacao do HCC sao pouco compreendidos.
Estudos previdos indicam mutacao em TERT, TP53, CTNNB1(Beta caterina).
Drogas terapeuticas no mercado são: sorofenib e regorafenib que são inibidoras de quinases.
- 26 genes sao significativamente mutados utilizando o algoritmo MutSigCV (GenePattern) (Laurance, 2014). Dentre eles:
TP53 (31%); AXIN1 (8%); RB1 (4%); CTNNB1 (27%); ARID1A (7%). genes da via WNT

- 44 de 196 (22,4%) evidências clinicas e moleculares de infecao HBV associado a forte etnia asiatica. são mutados em TP53.

- 37/44 84% tem evidencia de integracao de HBV (realizado por analise de dados de RNA-Seq).

HBV chr gene fusion transcripts.

35 de 196 (17.9%) HCV mais elevado em pessoas preto/branco em relacao a paciente asiaticos e em pacientes com necrose.

HCV+ CDKN2A mutados.

TP53 mutado tem o pior prognostico.

Fatores de risco: HCV, HBV, diabetes, obsidade e doencas metabolicas.

Check if the HBV+ samples which we find are the same in the TCGA classification.



## Papilomavirus

INCA. 
article: Characterization of HPV integration, viral gene expression and E6E7 alternative transcripts by RNA-Seq: A descriptive study in invasive cervical cancer.


* 
* /HPV-INCA 268Gb de informacao




# Questions/hyphotesis/things

* HBV integrado é mais presente em homens e no chr17
* HIVID base de dados???
* HBV muito presente na telomerase
* haplotype



# Tools to compare 

* [*V-pipe*: https://github.com/cbg-ethz/V-pipe](https://github.com/cbg-ethz/V-pipe)
* [*Vigen*: https://github.com/ICBI/viGEN/](https://github.com/ICBI/viGEN/)

-->

<!-- 
=====================
*** SANTOS DUMONT ***
=====================
---> 
# Santos Dumont Activitys 

    * rsem/1.3
    * samtools/1.9
    * anaconda XXX
    * bamtools/2.5.1
    * bedtools/2.29.0
    * blast/2.2.31
    * blast/2.9.0
    * bowtie2/2.3
    * bwa/0.7
    * gatk/4.1
    * star/2.7.3a
    * R/3.5.2_openmpi_2.0_gnu

## Slurm useful commands

    * Activity avail
    * Activity load NOME-DO-MODULO
    * Activity list
    * Activity unload NOME-DO-MODULO
    
  
Alguns programas e bibliotecas podem não estar configurados para serem carregados via aplicativo "Activity". Nesse caso, é necessário carregar as variáveis de ambiente da seguinte forma:

    * source /scratch/app/modulos/NOME-DO-MODULO ou . /scratch/app/modulos/NOME-DO-MODULO

Verificando status

* `squeue [parametros]` - Information about jobs and escalonamento. Exs: `$ squeue -p mesca2` `$ squeue -u raquel.costa2`
* `squeue --start` - Exibe o tempo esperado de quando um job entrará em execução (coluna START_TIME)
* `sinfo [parametros]` - Exibe informações sobre as filas/partições e os nós.
* `scontrol` show jobid #NUMERO-DO-JOB -dd - Exibe informações detalhadas sobre um job.

Remover jobs da fila ou em execução

    * scancel [parametros] #NUMERO-DO-JOB


# PARSL

Parsl is a flexible and scalable parallel programming library for Python useful for developing scientific workflows for high-performance computing environments. 
[https://parsl-project.org/](https://parsl-project.org/)

**Install Parsl:**

`python3 -m pip install parsl`


**Or installation using Conda**

1 - Create and activate a new conda environment:

`conda create --name parsl_py36 python=3.6`
`$ source activate parsl_py36`

2 - Install Parsl:

`$ conda config --add channels conda-forge`
`$ conda install parsl`

Documentation is available in [https://parsl.readthedocs.io/en/stable/](https://parsl.readthedocs.io/en/stable/)

[//]: # (**bold**)
